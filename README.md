# `vue-todo`

This is your typical beginner to-do app written in Vue.



## The Stack

*   **Vue 2**
*   **Vuetify 2** (for UI and styling)
*   **Vuex** (for data storage)
    *   `localStorage` is being considered, but may ultimately be…
        *   either dropped in favor of Vuex + Django REST (complete replacement); or
        *   used alongside Django REST (complementary)
*   **ShortUniqueId** (for generating unique IDs for lists and tasks. Pretty neat.)
*   **DayJS** (for transforming timestamps. Lightweight alternative to MomentJS & built-in Date functionalities from JS.)



## Getting Started

```sh
$ yarn
$ yarn serve

# Build for production
$ yarn build
```



## Room for Improvements

*   A ton.

