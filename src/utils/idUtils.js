import ShortUniqueId from "short-unique-id";

const _idGenerator = new ShortUniqueId({ length: 10 });

/**
 * Generate a random ID using `short-unique-id`.
 * @param {string} prefix String to be appended to the ID
 * @returns A random 10-character ID (with prefix if specified)
 */
export const genID = (prefix) => {
    return `${prefix === undefined ? "" : prefix}${_idGenerator()}`;
};
