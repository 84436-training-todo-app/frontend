import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

// import { lists as mockLists, tasks as mockTasks } from "@/store/task.mock";
import snaccbar from "@/store/snaccbar";

// import { cloneDeep } from "lodash-es";
import { findIndex, set, filter, omit } from "lodash-es";
import { deltaFromToday } from "@/utils/timeUtils";

Vue.use(Vuex);

const API_ENDPOINT_LIST = `${process.env.VUE_APP_API_ENDPOINT}/list`;
const API_ENDPOINT_TODO = `${process.env.VUE_APP_API_ENDPOINT}/todo`;

const state = {
    connectivityStatus: true,

    // lists: cloneDeep(mockLists),
    // tasks: cloneDeep(mockTasks),
    lists: [],
    tasks: [],

    currentListId: "00000000-0000-0000-0000-000000000000",
    currentlyOpenedTaskGroups: [1],
    currentSearchQuery: "",
};

const getters = {
    _currentListTasks: (state) => {
        return state.tasks
            .filter((each) => each.listUUID === state.currentListId)
            .filter(
                (each) =>
                    each.title.toLowerCase().includes(state.currentSearchQuery.toLowerCase()) ||
                    (each.desc && each.desc.toLowerCase().includes(state.currentSearchQuery.toLowerCase()))
            );
    },
    getConnectivityStatus: (state) => {
        return state.connectivityStatus;
    },
    pastTasks: (_, getters) => {
        return getters._currentListTasks.filter(
            (each) => !each.isDone && each.timestamp && deltaFromToday(each.timestamp) < 0
        );
    },
    todayTasks: (_, getters) => {
        return getters._currentListTasks.filter(
            (each) => !each.isDone && each.timestamp && deltaFromToday(each.timestamp) === 0
        );
    },
    futureTasks: (_, getters) => {
        return getters._currentListTasks.filter(
            (each) =>
                !each.isDone && ((each.timestamp && deltaFromToday(each.timestamp) > 0) || each.timestamp === undefined)
        );
    },
    doneTasks: (_, getters) => {
        return getters._currentListTasks.filter((each) => each.isDone === true);
    },
};

const actions = {
    checkConnectivity({ commit }) {
        axios.get(`${API_ENDPOINT_TODO}/`).catch(function () {
            commit("setConnectivityStatus", false);
        });
    },
    loadLists({ commit }) {
        axios.get(`${API_ENDPOINT_LIST}/`).then((res) => {
            commit("loadLists", res.data);
        });
    },
    loadTasks({ commit }) {
        axios.get(`${API_ENDPOINT_TODO}/`).then((res) => {
            commit("loadTasks", res.data);
        });
    },
    filterTasks({ commit }, payload) {
        commit("filterTasks", payload);
    },
    createTaskList({ commit }, payload) {
        const { title } = payload;
        axios.post(`${API_ENDPOINT_LIST}/create/`, { title }).then((res) => {
            commit("createTaskList", res.data);
        });
    },
    deleteTaskList({ commit }, payload) {
        const { uuid } = payload;
        axios.delete(`${API_ENDPOINT_LIST}/${uuid}/delete/`).then(() => {
            commit("deleteTaskList", uuid);
        });
    },
    createTask({ commit }, payload) {
        const { title, desc, timestamp } = payload;
        const newTaskPayload = {
            listUUID: state.currentListId,
            title,
            isDone: false,
        };
        if (desc) {
            newTaskPayload.desc = desc;
        }
        if (timestamp) {
            newTaskPayload.timestamp = timestamp;
        }
        axios.post(`${API_ENDPOINT_TODO}/create/`, newTaskPayload).then((res) => {
            commit("createTask", res.data);
        });
    },
    updateTaskStatus({ commit, state }, payload) {
        const { uuid, isDone } = payload;
        // Always set the title.
        const updatedTodoPayload = {
            title: state.tasks[findIndex(state.tasks, { uuid: uuid })].title,
            isDone,
        };
        axios.put(`${API_ENDPOINT_TODO}/${uuid}/update/`, updatedTodoPayload).then(() => {
            commit("updateTaskStatus", payload);
        });
    },
    updateTask({ commit }, payload) {
        const { uuid, title, desc, timestamp } = payload;
        // Always set the title.
        const updatedTodoPayload = { title };
        // To remove description and timestamp, set to "" (empty string)
        updatedTodoPayload["desc"] = desc ? desc : "";
        updatedTodoPayload["timestamp"] = timestamp ? timestamp : "";
        axios.put(`${API_ENDPOINT_TODO}/${uuid}/update/`, updatedTodoPayload).then(() => {
            commit("updateTask", payload);
        });
    },
    deleteTask({ commit }, payload) {
        // The payload is the UUID.
        const uuid = payload;
        axios.delete(`${API_ENDPOINT_TODO}/${uuid}/delete/`).then(() => {
            commit("deleteTask", uuid);
        });
    },
    setCurrentListId({ commit }, payload) {
        commit("setCurrentListId", payload);
    },
    setCurrentlyOpenedTaskGroups({ commit }, payload) {
        commit("setCurrentlyOpenedTaskGroups", payload);
    },
};

const mutations = {
    setConnectivityStatus(state, canConnect) {
        state.connectivityStatus = canConnect;
    },
    loadLists(state, lists) {
        state.lists = lists;
    },
    loadTasks(state, tasks) {
        state.tasks = tasks;
    },
    filterTasks(state, query) {
        state.currentSearchQuery = query;
    },
    createTaskList(state, taskListObj) {
        state.lists.push(taskListObj);
    },
    deleteTaskList(state, uuid) {
        // Find and remove all tasks in the store that matches list UUID
        state.tasks = filter(state.tasks, (each) => each.listUUID !== uuid);
        // Remove the list itself
        state.lists = filter(state.lists, (each) => each.uuid !== uuid);
    },
    createTask(state, taskObj) {
        state.tasks.push(taskObj);
    },
    updateTaskStatus(state, taskStatusInfo) {
        const { uuid, isDone } = taskStatusInfo;
        const targetIndex = findIndex(state.tasks, { uuid: uuid });
        state.tasks = set(state.tasks, `[${targetIndex}].isDone`, isDone);
    },
    updateTask(state, taskInfo) {
        const { uuid, title, desc, timestamp } = taskInfo;
        const targetIndex = findIndex(state.tasks, { uuid: uuid });
        let updatedTask = {
            ...state.tasks[targetIndex],
            title,
        };

        // Update/Remove desc
        if (desc) {
            updatedTask.desc = desc;
        } else {
            updatedTask = omit(updatedTask, ["desc"]);
        }

        // Update/Remove timestamp
        if (timestamp) {
            updatedTask.timestamp = timestamp;
        } else {
            updatedTask = omit(updatedTask, ["timestamp"]);
        }

        /* BEGIN 🚨 GIANT RED NEON SIGN
         * Vuex cannot react to mutation on state arrays (by index)
         * See this SO answer for workarounds: https://stackoverflow.com/a/41855691
         * END 🚨 GIANT RED NEON SIGN
         */
        Vue.set(state.tasks, targetIndex, updatedTask);
    },
    deleteTask(state, taskId) {
        state.tasks = filter(state.tasks, (each) => each.uuid !== taskId);
    },
    setCurrentListId(state, listId) {
        state.currentListId = listId;
    },
    setCurrentlyOpenedTaskGroups(state, openingGroups) {
        state.currentlyOpenedTaskGroups = openingGroups;
    },
};

export default new Vuex.Store({
    state,
    getters,
    actions,
    mutations,
    modules: {
        snaccbar,
    },
});
