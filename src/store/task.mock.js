import { genID } from "../utils/idUtils";

// TODO use a more sensible way to do stuff
const idListPlay = genID("list-");

export const lists = [
    { id: "list-0", name: "Default" },
    { id: idListPlay, name: "Play" },
];

export const tasks = [
    {
        id: genID("task-"),
        listId: "list-0",
        title: "Deadline trong ngày",
        timestamp: "2021-12-14T18:00",
        isDone: false,
    },
    {
        id: genID("task-"),
        listId: "list-0",
        title: "Task với description nè",
        desc: "Hù 👻",
        timestamp: "2021-12-14T23:00",
        isDone: false,
    },
    {
        id: genID("task-"),
        listId: "list-0",
        title: "Một cái task với kế hoạch siêu chi tiết",
        desc: "Kế hoạch là Super Idol 的笑容\n都没你的甜\n八月正午的阳光\n都没你耀眼\n热爱 105 °C的你\n滴滴清纯的蒸馏水",
        timestamp: "2021-12-16T23:00",
        isDone: false,
    },
    {
        id: genID("task-"),
        listId: "list-0",
        title: "Deadline ngày mai nè",
        timestamp: "2021-12-15T23:00",
        isDone: false,
    },
    {
        id: genID("task-"),
        listId: "list-0",
        title: "Xâm chiếm cả thế giới",
        isDone: false,
    },
    {
        id: genID("task-"),
        listId: "list-0",
        title: "Đi ngủ đủ 6h 💤",
        isDone: true,
    },
    {
        id: genID("task-"),
        listId: idListPlay,
        title: "Dọn cái desktop với thư mục download đi má ơi",
        desc: "Bừa bộn vl, bữa còn không kiếm thấy cái todo demo trên lớp mém nữa phải làm lại rồi",
        isDone: false,
    },
    {
        id: genID("task-"),
        listId: idListPlay,
        title: "Kiếm phim coi ik",
        desc: "Netflix Việt Nam bản free chán quá ._.",
        isDone: false,
    },
    {
        id: genID("task-"),
        listId: idListPlay,
        title: "Cày phim: Alice in Borderland",
        desc: "Nghe đồn là Squid Game bản máu me không kém :v",
        isDone: true,
    },
];
