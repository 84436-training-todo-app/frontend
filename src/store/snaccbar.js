export default {
    state: {
        text: "",
    },
    actions: {
        showSnaccbar({ commit }, payload) {
            commit("showSnaccbar", payload);
        },
    },
    mutations: {
        showSnaccbar(state, snaccInfo) {
            const { text } = snaccInfo;
            state.text = text;
        },
    },
};
